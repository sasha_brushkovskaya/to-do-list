module.exports = {
  port: 3000,
  // database: {
  //   url: '"mongodb://47.89.246.85:27017/"',
  //   dbName: 'to-do',
  //   collectionNotes: 'notes',
  // },

  database: {
    url: 'mongodb+srv://abrushkovskaya:alexandra5@cluster0-cs5w6.mongodb.net/test?retryWrites=true',
    dbName: 'To_do_app',
    collectionNotes: 'notes',
    collectionLists: 'To_do_list',
  },
};