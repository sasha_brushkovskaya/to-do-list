const conf = require('../config/server');

const mongodb = require('mongodb');
const MongoClient = require("mongodb").MongoClient;

const insertOne = async (collectionName, data) => {
  let client = await MongoClient.connect(conf.database.url, { useNewUrlParser: true });

  console.log('connected successfully! INSERT_ONE');

  const currentDb = client.db(conf.database.dbName);
  const worksCol = currentDb.collection(collectionName);
  const id = await worksCol.insertOne(data);

  await client.close();

  return id.insertedId;
};

const findMany = async (dbName, collectionName) => {
  let client = await MongoClient.connect(conf.database.url, { useNewUrlParser: true });

  console.log('connected successfully! GET');

  const currentDb = client.db(dbName);
  const usersCol = currentDb.collection(collectionName);
  const notes = await usersCol.find({}).toArray();

  await client.close();

  return notes;
};

const findOne = async (id, dbName, collectionName) => {
  let client = await MongoClient.connect(conf.database.url, { useNewUrlParser: true });

  console.log('connected successfully! GET_ONE');

  const currentDb = client.db(dbName);
  const usersCol = currentDb.collection(collectionName);
  const notes = await usersCol.findOne({ _id: new mongodb.ObjectID(id) });

  await client.close();

  return notes;
};

const updateOne = async (id, dbName, collectionName, data) => {
  let client = await MongoClient.connect(conf.database.url, { useNewUrlParser: true });

  console.log('connected successfully! UPDATE');

  const currentDb = client.db(dbName);
  const usersCol = currentDb.collection(collectionName);

  await usersCol.updateOne({ _id: new mongodb.ObjectID(id) }, { $set: data }, function (err, obj) {
    if (err) throw err;
    else console.log('Update!');
  });
  await client.close();
};

const deleteOne = async (id, dbName, collectionName) => {
  let client = await MongoClient.connect(conf.database.url, { useNewUrlParser: true });

  console.log('connected successfully! DELETE');

  const currentDb = client.db(dbName);
  const usersCol = currentDb.collection(collectionName);

  await usersCol.deleteOne({ _id: new mongodb.ObjectID(id) }, function (err, obj) {
    if (err) throw err;
    else console.log('Deleted!');
  });
  await client.close();
};

module.exports = {
  findMany: findMany,
  findOne: findOne,
  insertOne: insertOne,
  updateOne: updateOne,
  deleteOne: deleteOne
};