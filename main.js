const express = require("express");
const bodyParser = require('body-parser');
const db = require('./db/db');
const conf = require('./config/server');


const app = express();
const port = conf.port;

app.set('views', './views');
app.use('/', express.static('assets'));
app.use('/notes', express.static('assets'));
app.use('/lists', express.static('assets'));
app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", async function (request, res) {
  const notesArr = await db.findMany(conf.database.dbName, conf.database.collectionNotes);
  const listsArr = await db.findMany(conf.database.dbName, conf.database.collectionLists);


  res.render('index', { notesArr, listsArr });
});

app.get("/notes", async function (request, res) {
  res.render('notes-create', {
    title: 'Create Notes',
    content: ''
  })
});

app.get("/notes/:id", async function (req, res) {
  // console.log('id - ', req.params.id);
  const notes = await db.findOne(req.params.id, conf.database.dbName, conf.database.collectionNotes);
  // console.log(notes);
  res.render('notes-show', { notes })
});

app.post("/api/notes", async function (req, res) {
  await db.insertOne(conf.database.collectionNotes, req.body);
  res.send({ message: 'Notes have been saved!' });
});

app.put("/api/notes/:id", async function (req, res) {
  await db.updateOne(req.params.id, conf.database.dbName, conf.database.collectionNotes, req.body);
  res.send({ message: 'Notes have been updated!' });
});

app.delete("/api/notes/:id", async function (req, res) {
  await db.deleteOne(req.params.id, conf.database.dbName, conf.database.collectionNotes);
  res.send({ message: 'Notes have been deleted!' });
});

app.get("/lists", async function (request, res) {
  res.render('lists-create', {
    title: 'Create Lists',
    content: ''
  })
});

app.get("/lists/:id", async function (req, res) {
  const lists = await db.findOne(req.params.id, conf.database.dbName, conf.database.collectionLists);
  res.render('lists-show', { lists })
});

app.post("/api/lists", async function (req, res) {
  const id = await db.insertOne(conf.database.collectionLists, req.body);
  // res.send({ id: id });
  res.send({ message: 'List has been saved!' });
});

app.put("/api/lists/:id", async function (req, res) {
  const id = await db.updateOne(req.params.id, conf.database.dbName, conf.database.collectionLists, req.body);
  // res.send({});
  res.send({ message: 'List has been updated!' });
});

app.delete("/api/lists/:id", async function (req, res) {
  await db.deleteOne(req.params.id, conf.database.dbName, conf.database.collectionLists);
  // res.send('Ok!');
  res.send({ message: 'List has been deleted!' });
});

app.use("*", function(req, res){
  res.render('404');
});

app.use(function(err, req, res) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});


app.listen(port, function () {
  console.log(`Server running on port - ${port}.`);
});
