const getNotesParams = () => {
  return {
    color: $('.notes').css('background-color'),
    title: $('.notes__title-content').text(),
    description: [].map.call($('.notes__field'), item => $(item).text()).filter(item => item.length > 0)
  }
};

const modalShow = (message) => {
  $('#myModal').modal('toggle');
  $('.modal-title').text(message);
};

const checkParamsBeforeSave = (data) => {
  let str = data.title;
  data.description.forEach(item => str += item);

  if (!(str.split('')).find((item) => item !== String.fromCharCode(160) && item !== String.fromCharCode(32))) {
    modalShow('Can\'t save empty notes');
    return false;
  }
  return true;
};

const ajaxCallBack = (res) => {
  $('.btn-modal_close').css('display', 'none');
  $('.modal-header').css('justify-content', 'center');
  $('.spinner-border').css('display', 'none');
  modalShow(res.message);
  setTimeout(() => {
    window.location.href = "/";
  }, 500)
};

const ajaxPostPut = (type, data, id) => {
  $.ajax({
    type: type,
    url: id ? `/api/notes/${id}` : '/api/notes',
    data: JSON.stringify(data),
    contentType: 'application/json',
    dataType: 'json',
    success: ajaxCallBack
  });
};

const ajaxDelete = (id) => {
  $.ajax({
    type: 'DELETE',
    url: `/api/notes/${id}`,
    success: ajaxCallBack
  });
};

$('.btn').on('click', function () {
  const btn = $(this);
  const data = getNotesParams();

  switch (true) {

    case btn.hasClass('btn_save') && btn.hasClass('btn_edit'):
      if (checkParamsBeforeSave(data)) {
        $('.btn_edit > .spinner-border').css('display', 'inline-block');
        ajaxPostPut('PUT', data, ((window.location.pathname).split('/')).pop());
      }
      break;

    case btn.hasClass('btn_edit'):
      $(this).removeClass('btn-info').addClass('btn-success btn_save').text('Save').append('<span class="spinner-border spinner-border-sm"></span>');
      $('.notes__content').attr('contenteditable', true);
      $('.notes__title-content').attr('contenteditable', true);
      $('.dropdown-menu').removeClass('dropdown-menu_edit');
      break;

    case btn.hasClass('btn_save'):
      if (checkParamsBeforeSave(data)) {
        $('.btn_save > .spinner-border').css('display', 'inline-block');
        ajaxPostPut('POST', data);
      }
      break;

    case btn.hasClass('btn_trash'):
      $('.btn_trash > .spinner-border').css('display', 'inline-block');
      ajaxDelete(((window.location.pathname).split('/')).pop());
  }
});

$('.dropdown-menu').on('click', function (ev) {
  const bgColor = $(ev.target).css('background-color');
  $('.notes').css('background-color', bgColor);
});